#!/bin/bash

HOST_DESTINO=""

exibir_mensagem() {
    zenity --info --text="$1" --title="Gerenciador de Arquivos SCP"
}

obter_input() {
    zenity --entry --text="$1" --title="Gerenciador de Arquivos SCP"
}

obter_opcao() {
    zenity --list --column="$1" "${@:2}" --title="Gerenciador de Arquivos SCP"
}

realizar_copia() {
    arquivo_origem=$(obter_input "Digite o caminho completo do arquivo de origem:")
    arquivo_destino=$(obter_input "Digite o caminho completo do arquivo de destino:")

    scp "$arquivo_origem" "$arquivo_destino"

    if [ -z "$HOST_DESTINO" ]; then
        exibir_mensagem "O HOST_DESTINO não está configurado. Use a opção 'Salvar IP/Host' para definir o destino."
        return
    fi

    scp "arquivo_origem" "$HOST_DESTINO:$arquivo_destino"

    if [ $? -eq 0 ]; then
        exibir_mensagem "Cópia bem sucedida!"
    else
        exibir_mensagem "Erro ao realizar a cópia do arquivo."
    fi
     

}

verificar_integridade() {
    arquivo=$(obter_input "Digite o caminho completo do arquivo:")
    md5_original=$(md5sum "arquivo" | awk '{print $1}')

    zenity --info --text="Aguardando a cópia do arquivo ser concluída. Por favor, aguarde..." --title="Gerenciador de Arquivos SCP"

    scp "$arquivo" "$HOST_DESTINO:~/scp_temp"

    if [ $? -eq 0 ]; then
        md5_remoto=$(ssh "$HOST_DESTINO" "md5sum ~/scp_temp" | awk '{print $1}')

        if [ "$md5_original" == "$md5_remoto" ]; then
            exibir_mensagem "A integridade do arquivo foi verificada com sucesso!"
        else 
            exibir_mensagem "A integridade do arquivo foi comprometida."
        di 
    else 
        exibir_mensagem "Erro ao realizar a cópia do arquivo."
    fi

    ssh "$HOST_DESTINO" "rm ~/scp_temp"
    
}

exibir_lista_arquivos() {
    opcao=$(obter_opcao "Listar arquivos" "Cliente" "Servidor")

    if [ "$opcao" == "Cliente" ]; then 
        lista_arquivos=$ls
        exibir_mensagem "Lista de arquivos no cliente:\n$lista_arquivos"
    elif [ "opcao" == "Servidor" ]; then 
        lista_arquivos=$(ssh "$HOST_DESTINO" "ls")
        exibir_mensagem "Lista de arquivos no servidor:\n$lista_arquivos"
    else 
        exibir_mensagem "Opção inválida. Tente novamente."
        exibir_lista_arquivos
    fi
}

salvar_ip_host() {
    ip_host=$(obter_input "Digite o IP ou Host:")
    echo "$ip_host" > ip_host.txt
    exibir_mensagem "IP/Host salvo com sucesso!"
}

verificar_servidor_ssh() {
    ip_host=$(obter_input "Digite o IP ou Host:")

    ssh -q -o BatchMode=yes -o ConnectTimeout=5 "$ip_host" "echo 2>&1" > /dev/null

    if [ $? -eq 0 ]; then 
        exibir_mensagem "Servidor SSH está instalado e acessível em $ip_host."
    else
        exibir_mensagem "Não foi possível conectar ao servidor SSH em $ip_host."
    fi
}

principal() {
    opcao=$(obter_opcao "Opções" "Cópia de arquivo" "Verificar integridade" "Exibir lista de arquivos" "Salvar IP/Host" "Verificar servidor SSH")

    case $opcao in 
        "Cópia de arquivo")
            realizar_copia
            principal
            ;;
        "Verificar integridade")
            verificar_integridade
            principal 
            ;;
        "Exibir lista de arquivos")
            exibir_lista_arquivos
            principal 
            ;;
        "Salvar IP/Host")
            salvar_ip_host
            principal
            ;;
        "Verificar servidor SSH")
            verificar_servidor_ssh
            principal
            ;;
        "Sair")
            exit 0
            ;;
        *)
            exibir_mensagem "Opção inválida. Tente novamente."
            principal 
            ;;
    esac
}

if [ -f "ip_host.txt" ]; then
    HOST_DESTINO=$(cat ip_host.txt)
fi

principal
